const app = require('connect')();
const serveStatic = require('serve-static');
// const cors = require('cors');

// app.use(cors());

// Set header to force download
function setJsonHeaders(res, path) {
  res.setHeader('Content-type', 'application/json');
  // res.setHeader('Access-Control-Allow-Origin', '*');
}

// Serve up mock-api folder
app.use('/api', serveStatic('mock-api', {
  index: false,
  setHeaders: setJsonHeaders,
}));

// Serve up public folder
app.use('/', serveStatic('public', { index: ['index.html', 'index.htm'] }));

// app.use('/V1/categories/list', (serveStatic('mock-api/V1/categories')));
// app.use('/V1/categories/:id', serveStatic('mock-api/V1/categories'));

app.listen(8888, () => {
  console.log('Acesse: http://localhost:8888');
});

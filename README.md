# Instruções
- Instale as dependências
```
npm install
```
- Habilitar API (http://localhost:8888)
```
npm start
```
- Rodar Aplicação (http://localhost:3000)
```
npm run dev
```
## Tecnologias Utilizadas
```
- React para criação do frontend em SPA.
- Axios para consumir a API dada.
- CORS para habilitar tráfego entre origens diferentes.
(Caso navegador bloqueie será necessário instalação de plugins: Firefox - https://addons.mozilla.org/pt-BR/firefox/addon/cors-everywhere/ | Chrome: https://chrome.google.com/webstore/detail/allow-cors-access-control/lhobafahddgcelffkeicbaginigeejlf)
- CSS para customizar layout e responsividade.
```

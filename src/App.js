import React, { Component } from 'react';
import axios from 'axios';
import Home from './Components/Templates/Home';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCalled: false,
      list: [],
      camisetas: [],
      calcas: [],
      calcados: [],
    };
  }
  
  componentDidMount() {
    axios.all([
      axios.get('http://localhost:8888/api/V1/categories/list'),
      axios.get('http://localhost:8888/api/V1/categories/1'),
      axios.get('http://localhost:8888/api/V1/categories/2'),
      axios.get('http://localhost:8888/api/V1/categories/3'),
    ])
      .then(axios.spread((list, camisetas, calcas, calcados) => {
        this.setState({
          isCalled: true,
          list: list.data,
          camisetas: camisetas.data,
          calcas: calcas.data,
          calcados: calcados.data,
        });
      }))
      .catch(err => console.log(err));
  }

  render() {
    const {
      isCalled,
      list,
      camisetas,
      calcas,
      calcados,
    } = this.state;

    if (isCalled === false) {
      return <p>Not Loaded.</p>
    }
    return (
      <div className="App">
        <Home
          menu={list}
          camisetas={camisetas}
          calcas={calcas}
          calcados={calcados}
        />
      </div>
    );
  }
}

export default App;

import React, { Fragment } from 'react';
import { Link, NavLink, Route } from 'react-router-dom';
import Breadcrumbs from './Molecules/Breadcrumbs';

const Navbar = (props) => {
  const { links } = props;
  return (
    <Fragment>
      <nav>
        <div className="container">
          <ul className="d-flex flex-row-wrap">
            <li className="extrabold"><Link to="/">Página Inicial</Link></li>
            { links.items.map(item => (
              <li className="extrabold" key={item.id}>
                <NavLink to={item.path} className="nav-menu">{item.name}</NavLink>
              </li>
            )) }
            <li className="extrabold">Contato</li>
          </ul>
        </div>
      </nav>
      <Route exact path="/" render={() => <div className="container breadcrumb"><small>Página Inicial</small></div>} />
      <Route exact path="/calcados" render={(props) => <Breadcrumbs link={links.items[2]} />} />
      <Route exact path="/calcas" render={(props) => <Breadcrumbs link={links.items[1]} />} />
      <Route exact path="/camisetas" render={(props) => <Breadcrumbs link={links.items[0]} />} />

    </Fragment>
  );
};

export default Navbar;

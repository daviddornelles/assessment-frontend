import React from "react";
import { Route } from 'react-router-dom';

const Main = props => {
  const { link, product } = props;
  return (
    <div className="main-content">
      <div className="main-header">

        <Route exact path={`/${link.path}`} render={() => <h2>{link.name}</h2>} />

        <div className="main-subheader">
          <div className="order-icons">
            <span><i className="fas fa-th" /></span>
            <span><i className="fas fa-th-list" /></span>
          </div>
          <form className="order-by">
            <label htmlFor="order"><small className="extrabold">Ordernar Por</small></label>
            <select id="order">
              <option>Preço</option>
              <option>Marca</option>
              <option>Gênero</option>
            </select>
          </form>
        </div>
      </div>

      <div className="products">
        { product.items.map(item => (
          <div className="card" key={item.sku}>
            <figure>
              <img src={item.image} alt={item.name} />
            </figure>
            <p className="text-center title">{item.name}</p>
            <div>
              { item.specialPrice
                ? <p className="text-center"><span className="special-price"><small>R$ {item.price}</small></span> <span className="extrabold price">R$ {item.specialPrice}</span></p> 
                : <p className="text-center extrabold price">R$ {item.price}</p> }
              <button type="button" className="extrabold">Comprar</button>
            </div>
          </div>
        )) }
      </div>
    </div>
  );
};

export default Main;

import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => (
  <div className="container">
    <header>
      <span className="menu"><i className="fas fa-bars"><input type="checkbox" id="menu" /></i></span>
      <span className="logo">
        <Link to="/">
          <img src="images/webjump-logo.png" alt="webjump logo" />
        </Link>
      </span>
      <button type="submit" className="mobile"><i className="fas fa-search"></i></button>
      <form>
        <input type="text" />
        <button type="submit" className="search extrabold">buscar</button>
      </form>
    </header>
  </div>
);

export default Header;

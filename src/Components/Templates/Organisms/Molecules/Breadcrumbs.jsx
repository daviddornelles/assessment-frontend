import React from 'react';

const Breadcrumb = (props) => {
  const { link } = props;
  return (
    <div className="container breadcrumb">
      <small>
        Página Inicial &gt; <span>{link.name}</span>
      </small>
    </div>
  );
};

export default Breadcrumb;

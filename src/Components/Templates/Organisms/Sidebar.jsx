import React from 'react';
import { Link } from 'react-router-dom';

const Sidebar = (props) => {
  const { links } = props;
  return (
    <div className="sidebar">
      <h2>Filtre Por</h2>
      <h3>Categorias</h3>
      <ul>
        { links.items.map(item => (
          <li key={item.id}>
            <Link to={item.path} className="nav-menu">{item.name}</Link>
          </li>
        )) }
      </ul>
      <h3>Cores</h3>
      <div className="color-block red"></div>
      <div className="color-block orange"></div>
      <div className="color-block blue"></div>
      <h3>Tipos</h3>
      <ul>
        <li>Corrida</li>
        <li>Caminhada</li>
        <li>Casual</li>
        <li>Social</li>
      </ul>
    </div>
  );
};

export default Sidebar;

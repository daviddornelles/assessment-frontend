import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import Header from './Organisms/Header';
import Navbar from './Organisms/Navbar';
import Sidebar from './Organisms/Sidebar';
import Main from './Organisms/Main';
import Footer from './Organisms/Footer';


const Home = (props) => {

  const {
    calcas, calcados, camisetas, menu
  } = props;

  return (
    <Fragment>

      <Header />
      <Navbar links={menu} />

      <div className="container">
        <main role="main">
          <Sidebar links={menu} />

          <Route exact path="/calcados" render={(props) => <Main product={calcados} link={menu.items[2]} />} />
          <Route exact path="/calcas" render={(props) => <Main product={calcas} link={menu.items[1]} />} />
          <Route exact path="/camisetas" render={(props) => <Main product={camisetas} link={menu.items[0]} />} />
        </main>

        <Footer />

      </div>
    </Fragment>
  );
}

export default Home;
